"""
Run the Ellipsoid algorithm on linearly seperable data in R^2
Author: Noga Zaslavsky
"""

import matplotlib.pyplot as plt
import numpy as np

from ellie import EllipsoidLearner

# generate synthetic data
n = 50
X = np.zeros((2, n))
Y = np.zeros(n)
w = np.array([.5, .5])

for i in range(n):
    X[:, i] = [np.random.uniform(-1, 1) for j in range(2)]
    Y[i] = np.sign(w.dot(X[:, i]))

# plot data
plt.clf()
plt.plot(X[0, Y == 1], X[1, Y == 1], 'kd')
plt.plot(X[0, Y == -1], X[1, Y == -1], 'ko')
plt.plot([0, w[0]], [0, w[1]], 'kv-')
plt.plot([-1, 1], [0, 0], 'k-')
plt.plot([0, 0], [-1, 1], 'k-')
plt.plot([-1, 1], [1, -1], 'k--')
plt.ylim(-1, 1), plt.xlim(-1, 1)

# run the ellipsoid learner
learner = EllipsoidLearner(2)
learner.batch(X, Y)
print('Mistakes during training: ', learner.M)
print('Training error: ', learner.error(X, Y))

# plot results
w_hat = learner.w
Y_hat = learner.y_hat(X)
plt.plot([0, w_hat[0]], [0, w_hat[1]], 'gv-')
plt.plot(X[0, Y_hat == 1], X[1, Y_hat == 1], 'b.')
plt.plot(X[0, Y_hat == -1], X[1, Y_hat == -1], 'r.')
plt.show()
