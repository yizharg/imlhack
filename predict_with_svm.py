import os
import xml.etree.ElementTree
from random import sample

import numpy as np
import pandas
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline


def read_test(filename='/Users/yiz-mac/Downloads/sorted_data/automotive'):
    test = pandas.read_csv(os.path.join(filename, 'test.csv'), header=None)
    X, y = test[1], test[0]
    X = X.tolist()
    return np.array(X), np.array(y)


def load_all(filename='/Users/yiz-mac/Downloads/sorted_data/automotive'):
    e = xml.etree.ElementTree.parse(os.path.join(filename, 'negative.review')).getroot()
    neg_reviews = []
    neg_labels = []
    for atype in e.findall('review'):
        neg_reviews.append(atype._children[10].text)
        neg_labels.append(0)
    pos_reviews = []
    pos_labels = []
    e = xml.etree.ElementTree.parse(os.path.join(filename, 'positive.review')).getroot()
    for atype in e.findall('review'):
        pos_reviews.append(atype._children[10].text)
        pos_labels.append(1)

    reviews = pos_reviews[:130] + neg_reviews
    labels = pos_labels[:130] + neg_labels
    X, y = np.array(reviews), np.array(labels)
    return X, y


X, y = load_all()

l = len(X)
print l
f = l  # number of elements for train
indices = sample(range(l), f)

Xt, Yt = X[indices], y[indices]

Xtest, Ytest = read_test()

text_clf = Pipeline([('vect', CountVectorizer(lowercase=True)),
                     ('tfidf', TfidfTransformer()),
                     ('clf', SGDClassifier(loss='log', penalty='l1',
                                           alpha=1e-5, n_iter=100, random_state=12)),
                     ])
_ = text_clf.fit(Xt, Yt)

predicted = text_clf.predict_proba(Xtest)

print np.mean(predicted == Ytest)
