"""
Implementation of the Ellipsoid algorithm for online and batch learning
Author: Noga Zaslavsky
"""

import numpy as np


class EllipsoidLearner(object):
    # initialize the ellipsoid learner over R^d
    def __init__(self, d):
        self.d = d
        self.w = np.zeros(d)
        self.A = np.eye(d)
        self.M = 0  # mistakes

    # classify x
    def y_hat(self, x):
        return np.sign(self.w.dot(x))

    # online update step
    def online(self, x, y):
        yhat = self.y_hat(x)
        if y != yhat:
            self.M += 1
            Ax = self.A.dot(x)
            xAx = x.dot(Ax)
            self.w += y / ((self.d + 1) * np.sqrt(xAx)) * Ax
            eta = self.d ** 2 / (self.d ** 2 - 1.0)
            self.A = eta * (self.A - (2.0 / ((self.d + 1.0) * xAx)) * np.outer(Ax, Ax))
        return y != yhat

    # batch learning
    def batch(self, X, Y, max_itr=np.inf):
        d, n = X.shape
        stop = False
        itr = 0
        while not stop and itr < max_itr:
            stop = True
            itr += 1
            for t in range(n):
                err = self.online(X[:, t], Y[t])
                stop = stop and not err

    # evaluate error on a given data set (0-1 loss)
    def error(self, X, Y):
        y_hat = self.y_hat(X)
        return np.mean(y_hat != Y)
