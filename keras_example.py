import pickle as pcl

import numpy as np
import pandas
from keras.layers import Dense, Activation, Convolution1D, K, Lambda, Dropout
from keras.models import Sequential

import data_helpers

# set parameters:
max_features = 38763
maxlen = 190
batch_size = 32
nb_filter = 250  # the number of the convolution output. each convolution "square" can output nb_filter features. (since they create more layers)
filter_length = 1
hidden_dims = 250
nb_epoch = 1


def load_dataset(filename='tweets.csv'):
    train = pandas.read_csv(filename, header=None)
    X, y = train[1], train[0]
    X = X.tolist()
    return X, y


origx, origy = load_dataset()
x, y, vocabulary, vocabulary_inv = data_helpers.load_data(origx, origy)
pcl.dump(vocabulary, open("vocabulary.pcl", "wb"))
np.random.seed(100)
perm = np.random.permutation(np.arange(len(y)))
x_shuf = x[perm]
y_shuf = y[perm]
x_train, x_test = x_shuf[:-1000], x_shuf[-1000:]
y_train, y_test = y_shuf[:-1000], y_shuf[-1000:]

model = Sequential()

# we start off with an efficient embedding layer which maps
# our vocab indices into embedding_dims dimensions
# model.add(Embedding(max_features,
#                     190,
#                     input_length=maxlen,
#                     dropout=0.2))
x_train = x_train.reshape((x_train.shape[0], x_train.shape[1], 1))
x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], 1))
# we add a Convolution1D, which will learn nb_filter
# word group filters of size filter_length:
model.add(Convolution1D(nb_filter=nb_filter,
                        filter_length=filter_length,
                        batch_input_shape=(None, 190, 1),
                        border_mode='valid',
                        activation='relu'))


# we use max over time pooling by defining a python function to use
# in a Lambda layer
def max_1d(X):
    return K.max(X, axis=1)


model.add(Lambda(max_1d, output_shape=(nb_filter,)))

model.add(Dense(nb_filter))
model.add(Dropout(0.5))
model.add(Activation("relu"))

# We project onto a single unit output layer, and squash it with a sigmoid:
model.add(Dense(10))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
model.fit(x_train, y_train,
          batch_size=batch_size,
          nb_epoch=nb_epoch,
          validation_data=(x_test, y_test))

print model
