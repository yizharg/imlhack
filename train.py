#! /usr/bin/env python

import datetime
import os
import pickle as pcl
import time

import numpy as np
import pandas
import tensorflow as tf

import data_helpers
from neuralnetwork import NeuralNetwork

# Parameters
# ==================================================

# Model Hyperparameters
COMMIT_EVERY = 100
NUM_EPOCHS = 200
BATCH_SIZE = 50
dim_to_reduce = 20
filter_sizes = [1, 2]
num_filters = 50

def load_dataset(filename='tweets.csv'):
    train = pandas.read_csv(filename, header=None)
    X, y = train[1], train[0]
    X = X.tolist()
    return X, y


origx, origy = load_dataset()
x, y, vocabulary, vocabulary_inv = data_helpers.load_data(origx, origy)
# x_text = origx
# x_text = [data_helpers.clean_str(sent) for sent in x_text]
# x_text = [s.split(" ") for s in x_text]
pcl.dump(vocabulary, open("vocabulary.pcl", "wb"))
# pcl.dump(Counter(itertools.chain(*x_text)), open("vocabulary.pcl", "wb"))
np.random.seed(100)
perm = np.random.permutation(np.arange(len(y)))
x_shuf = x[perm]
y_shuf = y[perm]
x_train, x_test = x_shuf[:-1000], x_shuf[-1000:]
y_train, y_test = y_shuf[:-1000], y_shuf[-1000:]

with tf.Graph().as_default():
    sess = tf.InteractiveSession(config=tf.ConfigProto(inter_op_parallelism_threads=4,
                                                       intra_op_parallelism_threads=4))
    with sess.as_default():
        cnn = NeuralNetwork(
            sequence_length=x_train.shape[1],
            num_classes=10,
            vocab_size=len(vocabulary),
            dest_dimension=dim_to_reduce,
            filter_sizes=list(map(int, filter_sizes)),
            num_filters=num_filters)

        global_step = tf.Variable(0, name="global_step", trainable=False)
        # An improved version of SGD
        optimizer = tf.train.AdamOptimizer(0.001)
        # Gradiant descent the tf variables in order to minimize loss.
        grads_and_vars = optimizer.compute_gradients(cnn.loss)
        train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

        # Code piece to save our model and evaluate every certain steps:
        grad_summaries = []
        for g, v in grads_and_vars:
            if g is not None:
                grad_hist_summary = tf.histogram_summary("%s/grad/hist" % v.name, g)
                sparsity_summary = tf.scalar_summary("%s/grad/sparsity" % v.name, tf.nn.zero_fraction(g))
                grad_summaries.append(grad_hist_summary)
                grad_summaries.append(sparsity_summary)
        grad_summaries_merged = tf.merge_summary(grad_summaries)

        # Output directory for models and summaries
        timestamp = str(int(time.time()))
        out_dir = os.path.abspath(os.path.join(os.path.curdir, "final", timestamp))
        print("Writing to %s" % out_dir)

        # Summaries for loss and accuracy
        loss_summary = tf.scalar_summary("loss", cnn.loss)
        acc_summary = tf.scalar_summary("accuracy", cnn.accuracy)

        # Train Summaries
        train_summary_op = tf.merge_summary([loss_summary, acc_summary, grad_summaries_merged])
        train_summary_dir = os.path.join(out_dir, "summaries", "train")
        train_summary_writer = tf.train.SummaryWriter(train_summary_dir, sess.graph_def)

        # Dev summaries
        test_summary_op = tf.merge_summary([loss_summary, acc_summary])
        test_summary_dir = os.path.join(out_dir, "summaries", "dev")
        test_summary_writer = tf.train.SummaryWriter(test_summary_dir, sess.graph_def)

        # Checkpoint directory. Tensorflow assumes this directory already exists so we need to create it
        checkpoint_dir = os.path.abspath(os.path.join(out_dir, "checkpoints"))
        checkpoint_prefix = os.path.join(checkpoint_dir, "model")
        if not os.path.exists(checkpoint_dir):
            os.makedirs(checkpoint_dir)
        saver = tf.train.Saver(tf.all_variables())

        # Initialize all variables
        sess.run(tf.initialize_all_variables())


        def train_step(x_batch, y_batch):
            """
            A single training step
            """
            feed_dict = {
                cnn.input_x: x_batch,
                cnn.input_y: y_batch,
                cnn.dropout_keep_prob: 0.5
            }
            _, step, summaries, loss, accuracy = sess.run(
                [train_op, global_step, train_summary_op, cnn.loss, cnn.accuracy],
                feed_dict)
            time_str = datetime.datetime.now().isoformat()
            print("%s: step %s, loss %s, acc %s" % (time_str, step, loss, accuracy))
            train_summary_writer.add_summary(summaries, step)


        def test_step(x_batch, y_batch, writer=None):
            """
            Evaluates model on a dev set
            """
            feed_dict = {
                cnn.input_x: x_batch,
                cnn.input_y: y_batch,
                cnn.dropout_keep_prob: 1.0
            }
            step, summaries, loss, accuracy = sess.run(
                [global_step, test_summary_op, cnn.loss, cnn.accuracy],
                feed_dict)
            time_str = datetime.datetime.now().isoformat()
            print("%s: step %s, loss %s, acc %s" % (time_str, step, loss, accuracy))
            if writer:
                writer.add_summary(summaries, step)


        batches = data_helpers.batch_iter(
            list(zip(x_train, y_train)), BATCH_SIZE, NUM_EPOCHS)
        for batch in batches:
            x_batch, y_batch = zip(*batch)
            train_step(x_batch, y_batch)
            current_step = tf.train.global_step(sess, global_step)
            if current_step % COMMIT_EVERY == 0:
                print "Evaluation:"
                test_step(x_test, y_test, writer=test_summary_writer)
                print
            if current_step % COMMIT_EVERY == 0:
                path = saver.save(sess, checkpoint_prefix, global_step=current_step)
                print "Saved model checkpoint to %s" % path
