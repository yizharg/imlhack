#! /usr/bin/env python
# coding=utf-8
import pickle as pcl
from collections import Counter

import tensorflow as tf

import data_helpers

checkpoint_dir = "final_checkpoint"

"""
===================================================
     Introduction to Machine Learning (67577)
             IML HACKATHON, June 2016

            **  Tweets Classifier  **

Auther(s):
Alon Harel, 303078307
Yizhar Gilboa, 201041522

===================================================
"""


class Classifier(object):
    def __init__(self):
        self.vocabulary = Counter(pcl.load(open("vocabulary.pcl")))

    def classify(self, X):
        Xdat, y_test, vocabulary, vocabulary_inv = data_helpers.load_data(sentences1=X, vocabulary=self.vocabulary,
                                                                          maxlen=190)
        checkpoint_file = tf.train.latest_checkpoint(checkpoint_dir)
        graph = tf.Graph()
        with graph.as_default():
            sess = tf.Session()
            with sess.as_default():
                saver = tf.train.import_meta_graph("%s.meta" % checkpoint_file)
                saver.restore(sess, checkpoint_file)

                input_x = graph.get_operation_by_name("input_x").outputs[0]
                dropout_keep_prob = graph.get_operation_by_name("dropout_keep_prob").outputs[0]

                predictions = graph.get_operation_by_name("output/predictions").outputs[0]

                batch_predictions = sess.run(predictions, {input_x: Xdat, dropout_keep_prob: 1.0})
                return batch_predictions


if __name__ == '__main__':
    c = Classifier()
    X_test = ['There must be no new offshore drilling. Not now, not ever. http',
              '!!!!!!!!!!!! http',
              'REALLY UPSET About Trump &amp; Disabled Journalist‼️JUST HOW LONG Will This LYING  BULLY,B Allowed 2 Drag  Campaign 4  Pres. Of🇺🇸 I The GUTTER']
    print c.classify(X_test)
